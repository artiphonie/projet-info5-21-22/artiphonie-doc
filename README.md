Artiphonie - Documentation
====================================

*Vous trouverez dans ce répertoire l'ensemble des documents produits autour du projet, en fonction des années*  

Version 1 - 2020
--------------------------------------------------------

L'ensemble des documents réalisés la première année du projet se trouvent dans le dossier [2020](./2020/).

Version 2 - 2021
--------------------------------------------------------

L'ensemble des documents réalisés la deuxième année du projet se trouvent dans le dossier [2021](./2021/).

Version 3 - 2022
--------------------------------------------------------

Pour cette troisième année de projet,  l'ensemble des documents se trouvent dans le dossier [2022](./2022/).  

Vous pourrez y trouver :
- un dossier reprenant l'ensemble du processus de conception de l'interface d'administration (modèles de tâches, IHMs abstraites, maquettes) : [Conception Interface Admin](./2022/ConceptionInterfaceAdmin/).

- un document de justification des technologies utilisées : [Justification du choix de PrimeVue pour Vue3](./2022/ConceptionInterfaceAdmin/UIComponentLibraryJustification.md)

- un journal de bord reprenant l'ensemble de nos réunions au sein de l'équipe, et avec le client : [Journal De Bord](./2022/JournalDeBord.md).

- le pitch du projet, rédigé selon le cours "Méthodes de Projet Innovant" : [Pitch](./2022/Pitch.md).

- des posters réalisés à destination d'un public non technique, en anglais : [Poster (EN)](./2022/Poster%20Artiphonie_EN.png) ; et en français : [Poster (FR)](./2022/Poster%20Artiphonie_FR.png).

- ainsi qu'un flyer : [Flyer Artiphonie](./2022/Flyer_Artiphonie.pdf).

- le diapo de présentation du projet à mi-parcours : [Présentation de mi-parcours](./2022/Pr%C3%A9sentation%20mi-parcours%20-%20Artiphonie.pdf).

- un rapport présentant le résultat des 6 semaines de projet en 2022 : [Rapport final](./2022/Rapport%20final.md).

- un rapport technique présentant l'ensemble des documents pertinents en annexe du rapport principal : [Rapport technique](./2022/Rapport%20technique.md).  

- le diapo de présentation du projet lors de la soutenance finale : [Présentation finale](./2022/Pr%C3%A9sentation_finale_-_Artiphonie.pdf)

- un document reprenant l'ensemble des volontés du client, évoquées durant la version 3, mais non implémentées par faute de temps : [Poursuite du travail](./2022/Poursuite%20du%20travail.md).

- la spécification de la nouvelle base de données : [Spécification de la base de données](./2022/Sp%C3%A9cificationBDD.md).


Annexes
--------------------------------

Vous trouverez 2 autres dossiers comprenant les analyses des années précédentes : [Anciennes analyses](./Analysis/) ; et d'anciens rapports techniques : [Anciens rapports techniques](./Technical/).  

Enfin, un document décrivant l'étude RGPD des 3 années de projet est disponible : [RGPD](./RGPD.md).
