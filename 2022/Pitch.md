Pitch Artiphonie
================================================

---
Nous souhaitons vous présenter aujourd’hui ARTIPHONIE, la première application d’orthophonie personnalisable, intégralement en français, et disponible gratuitement sur le Store Androïd.

Une séance d’orthophonie au Centre de Référence des Troubles du Langage et des Apprentissages de Grenoble étant très courte, pour des enfants parfois atteints de troubles sévères du langage, cette application a pour but de permettre : premièrement, à l’enfant, de progresser plus rapidement, en travaillant depuis chez lui, tout en s’amusant ; puis, elle permet à l’orthophoniste de suivre l’évolution de ses patients à distance ; et enfin, aux parents d’être acteur de la progression de leur enfant.

L’application se compose de 3 menus, permettant à l’enfant de choisir entre APPRENDRE, S'ENTRAÎNER, et JOUER, chacun des menus étant adapté au trouble et à l’âge de l’enfant.
A côté, on retrouve un site web qui permet aux orthophonistes d’administrer l’application.

Par exemple, pour un enfant qui ne sait pas distinguer les sons “g” et “ch”, l’orthophoniste se rendra sur la fiche patient de l’enfant via la plateforme, et lui créera une liste adaptée à son trouble, afin qu’il travaille en particulier ces sons, dans différentes situations.  
Une fois que la liste est créée, l’enfant qui se rend sur sa tablette peut s’entraîner à reconnaître les nouveaux sons et mots choisis spécifiquement par l’orthophoniste. S’il souhaite s’amuser, plusieurs jeux lui permettront de poursuivre son apprentissage de manière ludique, car ils utilisent eux aussi la liste créée par l’orthophoniste.   
De son côté l’orthophoniste a accès à l’évolution de son patient sur les différentes activités. Elle peut alors décider de modifier la liste ou d’en créer une nouvelle. 

Ainsi, l’enfant est AUTONOME, et bénéficie d’un ACCOMPAGNEMENT SUR MESURE dans ses soins, ce qui en fait un projet avant tout, je dirais, HUMANISTE.  

---
