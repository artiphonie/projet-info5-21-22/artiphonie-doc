Poursuite du travail 
================================================

_Ce document reprend les différentes volontés du client, évoquées durant la version 3, mais non implémentées par faute de temps_ 

Objectifs prévus qui n'ont pas pu être réalisés - Prioritaires
--------------------------------
- lier l'interface d'administration au backend 
- lier l'application Artiphonie au backend 
- mise en place d'un serveur OVH
- disponibilité de l'application sur le Play Store 
- enregistrer les nouvelles consignes audio pour coller au texte
- insérer la banque de mots créée par Estelle dans l'interface d'administration (elle constituera le dictionnaire initial)
- ajouter un clavier phonétique à la fonctionnalité d'ajout d'un mot au dictionnaire, permettant à l'orthophoniste libéral d'inscrire l'écriture phonétique du nouveau mot
- finir de déplacer le niveau "phonétique" du jeu "Ecoute et choisis" dans la partie Entraînement
- tri par phonème à implémenter (une fois qu'il sera possible d'accéder à des phonèmes en Vue)
- implémenter l'envoi des statistiques de l'application Artiphonie à la plateforme, et à quelle fréquence ?
- tuto de téléchargement et installation de l'appli (si pas de PlayStore)


Objectifs pour la suite - Secondaires
---------
- droits d'accès à l'interface d'administration -> étape de connexion pour l'orthophoniste Admin d'abord + étapes de création des comptes orthophonistes libéraux par l'orthophoniste Admin + étape de connexion des orthophonistes libéraux sur la plateforme
- exportation des données résultats d'un patient 
- suppression d'un compte patient 
- implémenter une fonction de parsing de l'écriture phonétique d'un mot (pour en ressortir les phonèmes, familles de son, structures syllabiques du mot)
- ajouter la possibilité d'enlever un mot au dictionnaire 
- implémenter les fonctions de filtrage des autres filtres (pour l'instant, seul le filtre "nb de syllabes" est fonctionnel)  
- système d'envoi de mail à l'inscription d'un nouveau patient ou d'un nouvel orthophoniste dans la plateforme
- développement de nouveaux items de personnalisation de l'avatar
- mieux choisir les mots détracteurs dans le jeu "Ecoute et choisis" (mots de même filtrage par exemple)
- implémenter le gain d'étoiles dans la partie Entraînement  
- page About de l'interface d'administration 
- récompenser la régularité
