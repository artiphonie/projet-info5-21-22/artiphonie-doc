Choix d'une librairie de composants d'interface pour Vue3
================================================

Afin de faciliter le développement de la plateforme web pour les orthophonistes, codé en VueJS (version 3), nous avons souhaité nous aider d'une librairie de composants d'interface de type Bootstrap.  
Comme il en existe beaucoup, dont certaines non compatibles avec Vue3, il a fallu faire un comparatif d'efficacité.  

Voici le résultat de notre étude sur différentes librairies de composants d'interface, ainsi que la justification de notre choix final.  

| Nom de la librairie  | Installation | Intégration au code |
|-------------|------------|---------------------|
| PrimeVue |- ```npm install primevue@^3.12.1 --save```<br />- ```npm install primeicons --save ``` | - ```import PrimeVue from 'primevue/config';```<br />- ```app.use(PrimeVue);```
| Vuestic | - ```npm install vuestic-ui``` | - include fonts directly in your index.html’s head element<br />- ```import { VuesticPlugin } from 'vuestic-ui'```<br />- ```import 'vuestic-ui/dist/vuestic-ui.css'```<br />- ```app.use(VuesticPlugin)```
| Ionic | - ```npm install @ionic/vue @ionic/vue-router``` | - ```import { IonicVue } from '@ionic/vue';```<br />- ```app.use(IonicVue)```<br />- a lot of CSS imports
| Element+ | - ```npm install -D unplugin-vue-components unplugin-auto-import``` | ```import ElementPlus from 'element-plus'```<br />```app.use(ElementPlus)```<br />=> plus complexe |
  

| Nom de la librairie | Composants proposés | Popularité | Plus ou Moins |
|----------|---------|-----------|-------|
| PrimeVue | 99 composants | 2,1k | + : thèmes CSS gratuits, serveur discord |
| Vuestic | 51 composants | 1,2k | |
| Ionic | 96 composants | 46,5k | - : pas lié à Vue spécifiquement<br />+ : serveur discord, grande communeauté |
| Element+ | 68 composants | 14,5k | - : complexe, chinois |  

Ainsi, pour la diversité de ses composants et sa facilité d'utilisation, nous choisirons d'utiliser la librairie PrimeVue.
