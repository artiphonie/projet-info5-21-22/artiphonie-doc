Processus de création des Modèles de tâches
========================
  
Avant tout travail de conception, il est essentiel de commencer par détailler les fonctionnalités principales de l'application à développer ; puis de les découper en tâches, via l'exemple d'un parcours utilisateur type.   

C'est ce que l'on appelle un modèle de tâches. Il doit respecter des règles précises d'édition, au niveau de la nomenclature des tâches, de leurs caractères itératif/optionnel ; ou même au niveau des conditions nécessaires à leur réalisation.  

Nous avons réalisé nos modèles de tâches selon les règles apprises en INFO5, et avec le logiciel grenoblois **KMade**.  

Ci-dessous sont listées les fonctionnalités de base de l'interface d'administration que nous souhaitons développer, dont certaines (les plus complexes) ont donné lieu à un modèles de tâches :

S'inscrire
------------
L'inscription d'un orthophoniste sur la plateforme se fera par un orthophoniste désigné administrateur (dans un premier temps, Estelle Gillet-Perret, porteuse de ce projet). Les informations à fournir pour un orthophoniste seront dans un premier temps son nom-prénom et son adresse mail.    

De même que pour la création d'un compte patient, des identifiants de connexion seront générés par l'application, qu'il sera possible de modifier par la suite.
  
Se connecter
--------
Pour se connecter, l'orthophoniste doit rentrer un identifiant et un mot de passe.  
L'accès à un compte réduit lui permettra de modifier son mot de passe, son login ou toute autre information importante (comme l'adresse mail).  

Naviguer dans la plateforme 
--------------------------------------------------
Sitôt entré dans l'application, une page d'accueil nous laisse le choix entre créer une fiche patient, consulter la liste de ses patients, ou ajouter un mot dans le dictionnaire.  

Depuis le bandeau en haut de page, d'autres fonctionnalités comme le compte orthophoniste, (...) peuvent être aussi accessibles.

Administrer un compte orthophoniste  
---------------
L'accès au compte de l'orthophoniste se fera via le bandeau du site web.  
En plus de voir ses informations, il sera possible de les modifier (login, mdp, adresse mail) via un formulaire à valider.  

Créer un compte patient 
-----------  

![Créer un compte patient](TaskModels/Cr%C3%A9er%20un%20compte%20patient.png)

Chaque fiche patient doit être créée dans l'application, par l'orthophoniste qui suit le patient.  
Elle comprend les informations de base du patient, comme son nom, son prénom, son niveau scolaire et sa date de naissance.    

Dès la fiche patient validée, un login et un mot de passe seront automatiquement générés, puis envoyés à l'orthophoniste via mail. Ils seront également disponibles dans la fiche du patient.    

Ces mêmes identifiants serviront à inscrire l'enfant sur l'application Godot par la suite.  

Administrer un compte patient
-------------------------

![Administrer un patient](TaskModels/Administrer%20un%20compte%20patient.png)  

Pour accéder à la fiche d'un patient, l'orthophoniste devra d'abord rechercher le patient en question, soit en filtrant par son nom ou prénom grâce à la barre de recherche ; soit en navigant dans la liste de ses patients suivis.   

Cette liste sera visible sous la forme d'un catalogue de type système de fichiers, comprenant les informations indispensables à l'identification (comme le nom et le prénom), ainsi qu'un raccourci vers la liste de mots actuelle du patient.

Une fois accédé à la fiche patient, de nombreuses actions sont disponibles. Il est possible de modifier les informations du patient, d'accéder à ses anciennes listes de mots, et de créer une nouvelle liste de mots. Les listes de mots étant classées par date, nous avons choisi de n'autoriser les modifications que de la dernière liste de mots en date. Les autres listes peuvent uniquement être consultées. 

Enfin, en bas de la fiche se trouveront l'ensemble des statistiques récoltées sur le patient depuis l'application Artiphonie. 

Création d'une liste de mots pour un patient
--------------------------------------------------------

![Créer une liste de mots](TaskModels/Cr%C3%A9er%20une%20liste%20de%20mots.png)  

Nous partons du principe qu'une liste de mots sera créée par séance, suivant l'évolution du patient. Pour chaque nouvelle liste donc, il est possible de repartir d'une liste précédente, afin de poursuivre le travail progressivement ; de partir d'une liste générique, chacune adaptée à un travail particulier ; ou bien de partir d'un tableau vide.    

L'outil de création d'une liste consistera en une liste de filtres pré-définis par les orthophonistes. Ce sont des sons, des nombres de syllabes, des chaînes de caractères représentant la composition du mot en terme de consonnes-voyelles, ... En bref, tout ce qui sert à caractériser le mot en orthophonie. Pour chaque filtre, 3 actions seront possibles :
- OUI : dans ce cas, seuls les mots correspondants à l'attribut seront gardés
- NON : dans ce cas, seuls les mots ne correspondants pas à l'attribut seront gardés
- NEUTRE : dans ce cas, les mots ne sont pas filtrés en fonction de cet attribut, on garde de la même manière les mots qui correspondent, et qui ne correspondent pas
  
En fonction des attributs choisis et non choisis, le dictionnaire de mots sera filtré pour ne renvoyer que les mots correspondant à l'union de tous ces attributs entre eux.  La liste de mots récupérée est actualisée en temps réel.  
Enfin, la liste de mots générée en dernier consistera la nouvelle liste de travail du patient.

Ajouter un ou plusieurs mots dans le dictionnaire 
----------------------------  

![Ajouter un ou plusieurs mots dans le dictionnaire](TaskModels/Ajouter%20un%20ou%20plusieurs%20mots%20dans%20le%20dictionnaire.png)  

L'ajout d'un mot dans le dictionnaire pourra se faire depuis l'accueil. A chaque ajout, le mot doit être associé à une liste de caractéristiques orthophoniques (ses sons, sa représentation consonne-voyelle, son nombre de syllabes).

Nous souhaitons que cette action soit faite sur une page à part entière. Et qu'il soit possible d'ajouter plusieurs mots facilement à la suite.

----------   


Procéssus de création des IHM abstraites
=======================================================

Suite à la réalisation des modèles de tâches, on en déduit des IHM abstraites, sur lesquelles nous nous appuierons pour façonner des maquettes de la plateforme.    

Elles ont été réalisées avec **LucidChart**, une plateforme collaborative de création de diagrammes et de visualisation de données en ligne.


Créer un compte patient 
-----------  

![Créer un compte patient](AbstractIHMs/Cr%C3%A9er%20un%20compte%20patient.png)
 
Depuis l'accueil, un bouton nous permet d'accéder directement à la création d'un compte patient. Une fois validé, sa fiche patient nouvellement créée est affichée, ce qui permet notamment un accès facile aux nouveaux identifiants.

Administrer un compte patient
-------------------------

![Administrer un patient](AbstractIHMs/Administrer%20un%20compte%20patient.png)  
  
L'espace de la fiche d'un patient permet de nombreuses actions, dont beaucoup sont accessibles, sans se déplacer vers un espace dédié (comme l'observation)

Création et édition d'une liste de mots pour un patient
--------------------------------------------------------

![Créer une liste de mots](AbstractIHMs/Cr%C3%A9er%20une%20liste%20de%20mots.png)  
![Edition d'une liste de mots](AbstractIHMs/Editer%20une%20liste%20de%20mots.png)
  
La création d'une liste de mots se fait en 2 étapes : tout d'abord, il faut choisir à partir de quel support la nouvelle liste va être créée.  
Ensuite, l'édition consiste à modifier en parallèle le nom et le choix des filtres avant de valider une liste de mots qui nous convient.  

D'ailleus, cette liste de mots est actualisée et affichée en temps réel au fur et à mesure que l'on modifie les filtres.

Ajouter un ou plusieurs mots dans le dictionnaire 
----------------------------  

![Ajouter un ou plusieurs mots dans le dictionnaire](AbstractIHMs/Ajouter%20un%20ou%20plusieurs%20mots%20dans%20le%20dictionnaire.png)   

Pour ajouter un ou plusieurs mots au dictionnaire, un formulaire sera mis à disposition de l'orthophoniste, grâce auquel il pourra facilement enregistrer plusieurs mots les uns à la suite des autres.

----------------------------------------------------------------

Processus de création des Maquettes
=====================

Comme les IHMs abstraites décrivent plutôt la partie fonctionnelle de l'application, nous avons souhaité réaliser des maquettes de certaines pages de l'application web. Ainsi, nous pouvons mieux décrire notre vision de l'interface esthétique de l'application, et en discuter avec le porteur du projet.  

Les deux maquettes ci-dessous ont été particulièrement nécessaires, autant du côté business que développement, pour aider à mieux cerner les demandes et contraintes des deux parties, et ainsi mieux les satisfaire.   
  
Elles ont été réalisées avec **diagrams.net**, un logiciel de dessin graphique porté par Google.


Fiche patient
-----------  

![Fiche patient](Maquettes/Maquette%20Fiche%20Patient.png)
 
La fiche patient est la page sur laquelle seront regroupées toutes les informations se rapportant à un patient. On retrouve en haut à gauche son avatar et son identité (nom, prénom, date de naissance). Le niveau scolaire, ainsi qu'une description de son trouble peuvent également être affichés afin de mieux renseigner l'orthophoniste au premier coup d'oeil.  

On peut voir que dans le coin en haut à droite de l'identité du patient se trouve un bouton "Modifier". Ce bouton ouvre une fenêtre pop-up permettant de modifier ces informations.  

En haut à droite se trouvent un rappel des identifiants de connexion du patient, utiles pour son inscription sur l'application de jeu.  

Ensuite, on retrouve sur la gauche l'ensemble des actions liées aux listes de mots personnalisées (créer une nouvelle liste de mots, modifier la dernière liste de mots en date, et accéder aux anciennes listes de mots du patient). C'est aussi à cet endroit que l'orthophoniste pourra supprimer le compte du patient à la fin de son traitement.  

La plus grande place de cette page est enfin dédiée à la visualisation des résultats de l'enfant, que ce soit sous forme de graphiques, de chiffres, de camemberts. Les données récoltées sur l'activité de l'enfant seront entièrement choisies par les orthophonistes, pour leur pertinence et leur lisibilité.

Page de création d'une liste de mots
-------------------------

![Création d'une liste de mots](Maquettes/Maquette%20Page%20de%20tri.png)  
  
Nous avons souhaité rentrer plus en détail dans l'action de création d'une liste de mots personnalisée, car c'est la fonction principale de cette plateforme web.  

L'idée trouvée en commun avec l'orthophoniste et notre équipe est d'afficher l'ensemble des filtres sous la forme d'un système de fichiers. Plus on descend dans la hiérarchie, plus la personnalisation est fine.  

Pour plus de lisibilité néanmoins, et pour pallier au mieux les différentes manières de filtrer d'un orthophoniste à l'autre, nous avons opté pour des grandes catégories sur un même niveau de précision (ici, on peut voir le tri sur le nombre de syllabes, sur la structure syllabique, et sur les types de phonèmes qui sont au même niveau hiérarchique).   

Les feuilles de l'arbre sont toutes des coches à 3 états (sauf le tri sur le nombre de syllabes, qui est un nombre à entrer manuellement). Le premier état est neutre, c'est à dire que les mots ne seront pas triés en fonction de ce filtre. Le deuxième état, que l'on appellera OUI, et qui est représenté ici par un O, correspond à un filtrage positif : seuls les mots vérifiant cette condition seront gardés. Enfin, le troisième état est appelé NON, et est représenté par une X : seuls les mots ne vérifiant pas cette condition seront gardés. Ce qui permet une multitude de possibilité de tri, autant avec les phonèmes ou les structures que l'on ne veut pas faire travailler à l'enfant, qu'avec les phonèmes et les structures qui doivent être travaillées.  

L'écran à droite du système de filtrage représente les mots sélectionnés parmi le dictionnaire, mis à jour en temps réel.  

Enfin, la liste nouvellement créée doit être associée à un nom.




