# Rapport final


Plan
---
1. [Rappel du sujet](#Rappel-du-sujet)
2. [Objectifs](#Objectifs)
3. [Technologies employées](#Technologies-employées)
4. [Architecture technique](#Architecture-technique)
5. [Réalisations techniques](#Réalisations-techniques)
6. [Gestion de projet](#Gestion-de-projet)
7. [Outils](#Outils)
8. [Métriques logicielles](#Métriques-logicielles)
9. [Conclusion](#Conclusion)
10. [Retour d'expérience](#Retour-d'expérience)
11. [Bibliographie](#Bibliographie)


Rappel du sujet
---
Le projet Artiphonie, débuté en 2020, porte sur le développement d'une application mobile à destination des enfants atteints de troubles du langage. Réalisé en partenariat avec une orthophoniste du CHU de Grenoble, Artiphonie a 2 objectifs principaux : aider les enfants à améliorer leur prononciation de manière autonome, en dehors des séances d’orthophoniste ; permettre aux orthophonistes de suivre la progression de leurs patients et d’adapter les exercices à leurs troubles. Des applications similaires existent déjà, mais sont soit payantes soit disponibles uniquement pour d’autres langues. L’idée est donc de créer une application  gratuite et collaborative pour les orthophoniste français.

Objectifs
---
Dès le début du projet, nous avons pu définir une série d'objectifs en fonction des besoins de l'orthophoniste et porteuse du projet, Mme Gillet-Perret. Nos objectifs pour cette année sont donc les suivants :

* Permettre à l’orthophoniste de personnaliser la banque de mots en fonction du trouble de l’enfant
* Rendre l’application disponible sur le Play Store, afin de faciliter son installation par les familles
* Développer l’interface administrateur (site web) pour les orthophonistes (connexion à ses patients, upload du jeu de données personnalisé par patient, visualisation des statistiques de chaque patient)
* Refonte de l’architecture backend (API, BD)
* Ajout de consignes pour les parents
* Retrait de la reconnaissance vocale, dans la partie entrainement. La validation est assurée par un adulte

Technologies employées
---

### Godot - GDScript
Pour développer l'application sur tablette Android, nous avons utilisé le moteur de jeu Godot, déjà utilisé par les précédents groupes sur le projet. Il permet notamment de définir des scènes types (templates), afin de rendre l'application facilement extensible à l'avenir. Le langage utilisé est le GDScript, propre au moteur de jeu.

### VueJS - PrimeVue
Pour développer l'interface d'administration pour les orthophonistes, accessible depuis le web, nous avons utilisé le framework JavaScript VueJS. Etant la première équipe à la développer, le framework VueJS (version n°3) s'est imposé pour sa simplicité et sa légèreté. Il a fallu par la suite choisir la librairie de composants à utiliser pour nous aider dans la réalisation de belles interfaces, et compatible avec Vue3. Le document décrivant notre étude est disponible dans notre [Rapport technique](./Rapport%20technique.md). Il en résulte le choix de la librairie PrimeVue, pour son nombre de composants, sa simplicité d'utilisation, et sa documentation détaillée.

### Python - SQLite - FastAPI
Depuis la preuve de concept des étudiants de l'année dernière, portant sur un backend en Java Spring communiquant avec une base de données PostGreSQL, les technologies ont été entièrement changées. Cette année, nous avons donc repris le développement d'une API et d'un backend, de zéro, en Python. Nous utilisons tout d'abord FastAPI, un framework Python permettant de créer des APIs. Pour dialoguer avec la base de données en SQLite, nous utilisons l'outil SQLAlchemy. Il joue le rôle d'Object Relational Manager (ORM).
  
### Docker
Enfin, pour simplifier le déploiement et le développement des interfaces frontend, nous avons choisi de faire tourner ce serveur backend dans un conteneur Docker. Cela permet aux développeurs frontend de ne pas se soucier des dépendances nécessaires au bon fonctionnement du serveur backend. 


Architecture technique
---
L'architecture globale du projet n'a pas beaucoup evolué depuis la dernière itération d'Artiphonie. Ainsi la vue d'ensemble suivante est toujours d'actualité. Elle représente bien chacun des composants du projet.
![Diagramme vue d'ensemble](./Rapport%20final%20-%20Images/Diagramme%20vue%20d'ensemble.png)    

L'enfant et le parent intéragissent avec l'application Godot sur tablette. Les orthophonistes suivent leurs patients depuis une plateforme web en VueJS.   
De leur côté, un service Python et une base de données SQLite communiquent entre eux grâce à une API FastAPI, afin de répondre aux demandes des applications front.

![Diagramme Architecture backend](./Rapport%20final%20-%20Images/Diagramme%20Architecture%20backend.png)

Réalisations techniques
---  

### Application Godot
Comme dit précédemment, Godot est le moteur de jeu que nous avons utilisé pour continuer d'améliorer l'application afin de répondre aux différentes demandes du client.

#### Interface de connexion
![Feature connexion](./Rapport%20final%20-%20Images/Feature%20connexion%20sur%20l'application%20de%20jeu.png)  

Pour permettre aux patients de récupérer les différentes données qui leur sont propres, il a fallu concevoir un système de connexion à partir de l'application sur tablette. Deux modes de fonctionnement ont donc été pensés, un fonctionnement avec et sans connexion. Comme montré sur l'image ci-dessus, les deux modes de fonctionnement sont accessibles grâce à une nouvelle interface.

Le mode avec connexion internet permet plusieurs choses. Suite à une demande de connexion à la base de données, les identifiants sont analysés par cette dernière afin de renvoyer les informations tel que la liste des mots à travailler, le niveau de progression du joueur ... Ces informations sont alors chargées dans l'application et sauvegardées dans un fichier JSON local. Dans ce mode, l'application envoie à chaque fin d'exercice une requête à la base de données contenant un rapport des mots qui n'ont pas été bien prononcés, la date, le taux de réussite, le nom de l'exercice ainsi que la difficulté dans laquelle ce dernier a été réalisé.

Le mode sans connexion permet de gérer des cas où l'utilisateur ne pourrait pas accéder à une connexion internet. Les principales différences avec le mode avec connexion sont : 
- Le chargement des données directement depuis le JSON et non pas avec la réception de la réponse du backend.
- La non-possibilité d'envoi de résultats à la base de données.  

#### Gestion du déblocage des difficultés
![Feature blocage des difficultés](./Rapport%20final%20-%20Images/Feature%20blocage%20des%20niveaux%20de%20difficult%C3%A9.png)  

Une des demandes de l'orthophoniste était de limiter l'accès aux différents niveaux de difficultés de chaque jeu. Le but étant que chaque exercice soit suffisamment réussi avant de permettre au patient d'accèder au prochain. Pour débloquer le prochain niveau de difficulté, le patient doit dépasser un taux de réussite d'au moins 80%. Les différents niveaux de difficultés sont stockés en local et sauvegardés à chaque fin de partie.  

#### Passage d'un système de récompense à niveau
![Feature blocage des niveaux de difficulté](./Rapport%20final%20-%20Images/Feature%20syst%C3%A8me%20de%20r%C3%A9compense.png)  

Dans Artiphonie, le joueur accumule des étoiles. Elles correspondaient auparavant à une monnaie permettant au patient de pouvoir débloquer des items afin de personnaliser son avatar. Le nouveau système de récompense est un système plaçant désormais les étoiles comme un niveau de progression, chaque item ayant un niveau d'étoiles requis afin d'être débloqué automatiquement.  

#### Modification de la modalité de validation
La vision autour de l'utilisation de l'application a évolué d'une année à l'autre. Auparavant, on considérait qu'il valait mieux permettre une utilisation en autonomie de l'application afin d'encourager le patient à s'entraîner au maximum. Pour ce faire, la validation de la prononciation  d'un mot était effectuée automatiquement par l'assistant Google, cependant, ce dernier posait problème. En effet, cette technologie n'est pas exacte, et il se peut que la reconnaissance se fasse mal alors que le mot a été correctement prononcé, ce qui peut être démotivant lors d'une séance d'entraînement.  

![Feature validation_avant](./Rapport%20final%20-%20Images/Feature%20validation%20par%20les%20parents_avant.png)  

Le bouton microphone est le bouton permettant de lancer la reconnaissance vocale. Il a alors été décidé de changer ce dernier et de le remplacer par un nouveau système de validation se basant sur l'accompagnement d'un parent.  

![Feature validation_après](./Rapport%20final%20-%20Images/Feature%20validation%20par%20les%20parents_apr%C3%A8s.png)  

Comme sur l'image ci-dessus, les parents disposent de deux boutons afin de signifier à l'application si le patient a correctement pronnoncé le mot, les parents prennent alors une place nettement plus importante désormais, ils jouent un vrai rôle d'accompagnants.  

![Feature consignes parents](./Rapport%20final%20-%20Images/Feature%20consignes%20parent.png)  

Pour les aider, il a été pensé de rajouter un nouveau bouton à l'interface principale servant d'accès aux consignes leur indiquant la bonne utilisation de l'application.

### Interface Administrateur
  
L'interface d'administration est une des exigences du projet Artiphonie, pour permettre aux orthophonistes de suivre leur patient tout au long de leur travail, en leur proposant des listes de mots adaptées à leur trouble et en visualisant leurs résultats à distance. Cependant, par faute de temps, elle n'avait pas été commencée par les 2 précédentes équipes, qui ont préférées s'occuper de l'application principale, à destination des patients.  

Cette année, nous avons donc dû la concevoir intégralement, avant de débuter son développement.

#### Conception

Les étapes de conception ont été : 
- la réalisation de modèles de tâches pour les tâches **Créer un compte patient**, **Administrer un compte patient**, **Créer une liste de mots**, et **Ajouter un mot au dictionnaire**
- la réalisation d'IHMs abstraites à la suite des modèles de tâches
- la réalisation de maquettes pour les pages **Fiche patient** et **Création d'une liste de mots**  

Un document détaillant ces étapes est disponible dans notre [Rapport technique](./Rapport%20technique.md).

#### Réalisation

Une fois l'étape de conception bien avancée, il a fallu commencer à développer en pratique les interfaces théorisées.  

Dans un premier temps, nous avons décidé de ne faire que des interfaces indépendantes, c'est à dire non reliées au backend, qui était en réfaction tout au long de ce projet. Pour ce faire, de fausses données ont été imaginées et insérées à chaque fois que nécessaire, pour mocker au mieux le dynamisme réel de l'application.  

Voici la liste des pages développées, ainsi que leurs premières fonctionnalités :  
- **page d'accueil de la plateforme** : sur cette page se trouvent l'ensemble des accès aux autres pages du site. Dans le bandeau se trouve l'accès au compte de l'orthophoniste, via lequel il est possible soit de modifier ses informations, soit de se déconnecter. Dans le bas de page, des liens vers une page de "à propos" et de métriques sont disponibles. Dans l'encadré principal de la page, on retrouve la possibilité de créer un compte patient et d'ajouter un mot au dictionnaire (ces deux boutons amènent chacun vers une fenêtre pop-up spécifique à l'action, et non vers une nouvelle page, ce qui a pour intérêt de ne pas perdre l'utilisateur en navigant). Enfin, une barre de recherche et la liste des patients de l'orthophoniste connecté permettent de facilement accéder à la fiche d'un patient.  

![Feature page d'accueil](./Rapport%20final%20-%20Images/Feature%20Page%20d'accueil.png)

- **page du compte orthophoniste** : cette page sert essentiellement à consulter et modifier les informations de l'orthophoniste, par exemple son adresse mail.  

![Feature Compte orthophoniste](./Rapport%20final%20-%20Images/Feature%20Compte%20orthophoniste.png)

- **page de la fiche patient** : comme décrite dans la partie Conception, cette page sert à centraliser les informations sur un patient. On y retrouve son identité (nom, prénom, date de naissance, niveau scolaire), ses identifiants de connexion (servant à se connecter sur l'application de jeu), ses résultats (un ensemble de graphiques permettent de visualiser les données recueillies sur l'application de jeu, comme le temps de jeu, les activités les plus jouées, les plus réussies, les mots échoués, la régularité, le nombre d'étoiles, etc ...). Aussi, 3 actions principales sont accessibles, comme la création d'une liste de mots, la modification de la liste de mot actuelle, et l'accès aux anciennes listes de mots du patient. Pour l'instant, seule la fonction "création d'une nouvelle liste de mots" a été gérée. Au click sur le bouton, une fenêtre pop-up demande à l'utilisateur le *template* de la liste de mots voulue. Il a la possibilité de choisir entre créer sa liste de mots à partir d'une ancienne liste du patient, à partir d'une liste générique, globale à tous les orthophonistes et designée pour travailler un son particulier, ou de la créer tout simplement à partir d'une liste vide.  

![Feature Fiche patient](./Rapport%20final%20-%20Images/Feature%20Fiche%20patient.png)

- **page de création d'une liste de mots** : cette page fait suite à l'action décrite précédemment, accessible depuis la fiche d'un patient. Sur cette page, une liste de filtres est affichée sous la forme d'un système de fichiers. A droite, la liste des mots filtrée est affichée en temps réel. Il est possible de la valider en la nommant, pour revenir à la fiche patient. Pour l'instant, seul le filtre *nombre de syllabes* a été implémenté, c'est à dire que la liste de mots n'affiche que les mots ayant un nombre de syllabes inférieur ou égal au nombre demandé. Un exemple de fausses données a été utilisé pour cette page, notamment une architecture de filtres, ce qui facilitera le travail de liaison avec la base de données plus tard.

![Feature Page de création d'une liste](./Rapport%20final%20-%20Images/Feature%20Page%20de%20cr%C3%A9ation%20d'une%20liste.png)


Au final, nous n'avons pas eu le temps de relier ces interfaces aux données réelles du projet Artiphonie. 

### Backend

Le backend de Artiphonie 2 avait été jugé peu adapté aux besoins de l'application, c'est pourquoi nous avons refait tout le backend de 0. Même si nous avons pu reprendre certains travaux de nos prédécesseurs et que nous nous sommes inspirés de leurs preuves de concept pour la conception du backend (notamment la BDD), tout a été revu en fonction des besoins des orthophonistes. Ainsi la BDD a été complêtement actualisée, et les API changées.  

#### Base de données

Pour Artiphonie 3, la base de donnée utilise dorénavant SQLite. Il s'agit d'une base de données relationnelle qui n'a pas besoin de connexion avec un serveur. Un simple fichier suffit pour le stockage des données. Pour cette itération du projet, il nous fallait une base de donnée légère avec des intéractions rapides.

Dans un second temps, le module Python SQLAlchemy permet de mapper des objets avec des tables SQL. Ce module assure tous les échanges avec SQLite et agit comme intermédiaire entre FastAPI et SQLite.  

![Diagramme nouvelle BDD](./Rapport%20final%20-%20Images/Diagramme%20nouvelle%20BDD.png)    

Vous trouverez une explication détaillée des tables de cette nouvelle base de données dans notre [Rapport technique](./Rapport%20technique.md).

#### API

Tout le backend fonctionne avec python3, et plus particulièrement avec le framework web FastAPI, qui nous fournit des méthodes pratiques pour développer facilement des API web.

Nous avons dressé une liste des API nécessaires au bon fonctionnement de l'application web et de l'appli tablette.

![Diagramme API](./Rapport%20final%20-%20Images/Diagramme%20API.png)


Gestion de projet
----  

### Méthode 

Nous avons décidé d'appliquer la méthodologie Agile vue en cours. Nous avons choisi la méthode SCRUM car elle s'adapte bien à notre projet et à la taille de notre équipe groupe (pizza team).   

Le projet a été découpé en 4 sprints de 1 à 2 semaines. Pour chaque sprint, nous avons prévu une série de tâches à réaliser. Grâce à l'agilité, nous avons pu adapter la charge des sprints pour rattraper des retards éventuels.  

L'équipe se réunissait 3 fois par semaine en fin de matinée (Lundi, Mercredi, Vendredi) pour échanger sur nos avancées, nos réalisations et nos points de blocage. Côté client, 2 réunions ont été organisées avec Estelle Gillet-Perret directement au CRTLA, ainsi que plusieurs autres par téléphone, tout au long du projet. On compte 2 réunions avec Olivier Richard au début du projet, pour mettre au point les objectifs de la nouvelle année, notamment au niveau backend.

### Planning prévisionnel VS Planning effectif  
_Planning prévisionnel_  

![Planning prévisionnel](./Rapport%20final%20-%20Images/Planning%20pr%C3%A9visionnel.jpg)
  
_Planning effectif_  

![Planning effectif](./Rapport%20final%20-%20Images/Planning%20Effectif.png)

En conclusion, on peut voir que la prise en main des technologies et la conception ont pris plus de temps que prévu. On peut relier ce ralentissement à la présence en parallèle et pour tous les membres de l'équipe d'un deuxième projet à plein temps, qui s'est terminé le 1er mars.  

Une fois ce projet terminé, le développement bât son plein ! En comparaison, la fonctionnalité de filtrage et de personnalisation des listes de mots pour l'enfant a été réalisée dans le Sprint 3 au lieu du Sprint 2. En cause, la mise en place dans un premier temps de l'architecture globale de l'interface web, et le long processus de communication client, qui nous a permis d'obtenir un résultat au plus proche du souhait du client.  

Enfin, le 3ème Sprint s'est focalisé sur le backend, pour lequel les premiers Sprints ont servi d'apprentissage et de réflexion à une réfaction importante de toute la structure et de toutes les technologies existantes. La rédaction de documents de passation et de rapports ont aussi pris une petite partie du Sprint.

### Gestion des risques 

La gestion des risques a été réalisée naturellement. De par les outils utilisés (Discord, Gitlab) ou de l'application de la méthode Scrum, nous avons pu couvrir en majorité les risques mineurs.   

En revanche, nous avons omis certains risques que nous ne pensions pas rencontrer mais qui sont vite apparus : Covid, déplacements impératifs, qui ont conduit à l'indisponibilité de certains membres du groupe. Même si nous n'étions pas préparés à traiter ces problemes, nous fûmes en mesure de les traiter efficacement, et que ça ne nuise pas à la dynamique de travail général. Les membres concernés rattrapaient leur travail plus tard ou s'occupaient de tâche moins chronophage à la place.

### Rôles des membres  
![Rôle des membres](./Rapport%20final%20-%20Images/R%C3%B4le%20des%20membres.png)


Outils
---

### Outils de collaboration

<p style="width: 80px"><img src="./Rapport%20final%20-%20Images/Logo%20Discord.png"></p>

La totalité des communications internes de l'équipe se sont fait sur Discord. Cet outil nous a permis de communiquer rapidement et efficacement sur n'importe quel tâche du projet à tout moment.

<p style="width: 50px; margin-left: 15px"><img src="https://i.imgur.com/OZJZrdl.png"></p>  

L'outil de version par excellence. Il a permis de synchroniser tout le code produit et de bien segmenter le code dans les répertoires. De même il nous a permis de représenter au mieux les méthodes agiles (scrum) via son système d'*issues*.

<p style="width: 50px; margin-left: 15px"><img src="https://i.imgur.com/GtyGEW0.png"></p>

Que ce soit pour la rédaction des documents de conception, de rapport ou des présentations, les différents services offerts par Google furent utilisés efficacement.

<p style="width: 70px; margin-left: 5px"><img src="https://i.imgur.com/0JfgY4w.png"></p>

C'est grâce à cet outil que nous avons réalisé le poster, en anglais comme en français. Canva propose la possibilité de travailler à plusieurs sur un même document.

<p style="width: 50px; margin-left: 15px"><img src="https://i.imgur.com/DqsqDDL.png"></p>

Utilisé pour faire les maquettes ou encore le schéma de la nouvelle base de données, LucidChart accorde tout comme Canva la possibilité de travailler en même temps sur le même document. 

<p style="width: 50px; margin-left: 15px"><img src="https://i.imgur.com/OenpY1n.png"></p>

Cet outil collaboratif permettant de rédiger du markdown en ligne à plusieurs nous a servi pour la rédaction des rapports.

### Outils CI/CD  

Nous avons fait le choix de ne pas mettre en place d'intégration continue par manque de temps et de vision globale au début du projet.

Il aurait été utile de développer une chaine de déploiement continu, malheureusement nous n'avons pas pu avoir d'accès à un serveur OVH ni à un compte développeur Play Store. 
- L'accès à un serveur OVH aurait été utilisé pour déployer le site web d'administration
- Le compte développeur Play Store nous aurais permis de mettre en ligne l'application Android sur le Google Play Store. 

N'ayant pas d'endroit où déployer nos applications, nous n'avons donc pas mis en place de déploiement continu. Pour faciliter le déploiement futur du serveur backend, nous avons pris l'initiative de le conteneuriser avec Docker.


Métriques logicielles
---  

### Lignes de code  

#### Lignes de codes par personne par repository
Les résultats par étudiant sont sous le format nombre de lignes écrites / lignes supprimées
##### Application Godot
Julien : 980 / 598
Daphné : 151304 / 37

##### Backend
Paul : 373 / 139
Sami : 776 / 300
Daphné : 5 / 0

##### Interface administrateur
Daphné : 5819 / 834
Paul : 13752 / 2

#### Lignes de code par personne par jour en moyenne 
Daphné : 3142,56
Julien : 19,6
Paul : 282,5
Sami : 15,52

La quantité de lignes de code produites est à prendre avec un peu de recule quand on sait que certaines technologies utilisées génèrent du code ou bien encore l'import de certaines librairies qui peuvent augmenter drastiquement le montant de lignes.


### Langages  

Répartition des technologies au sein des trois projets : 

#### Application Godot
GDscript : 84.5 %
Java : 14.7 %
Python : 0.7 %
GLSL : 0.2 %

#### Backend
Python : 99 %
Dockerfile : 1 %

#### Interface administrateur
Vue : 94.8 %
Javascript : 3.9 %
HTML : 1.3 %


### Performances de l'équipe  

#### Nombre d'issues par personne par repository

##### Application Godot
Julien : 7 
Daphné : 3

##### Backend
Paul : 4
Sami : 5

##### Interface administrateur
Daphné : 7 
Paul : 1 

#### Nombre d'issues par personne par jour

Daphné : 0.2
Julien : 0,14
Paul : 0.1
Sami : 0.1

#### Nombre de commits par personne par repository

##### Application Godot
Julien : 29 
Daphné : 17

##### Backend
Daphné : 1
Paul : 21
Sami : 19

##### Interface administrateur
Daphné : 27 
Paul : 8 

#### Nombre de commits par personne par jour

Daphné : 0.9
Julien : 0,58
Paul : 0.58
Sami : 0.38

### Temps ingénieur  

Nous nous sommes fixés un temps de travail standard de 35h par semaine en moyenne sur le projet et avons respecté cette règle jusqu'à la fin du projet (sauf en cas d'absence). Par conséquent, sur la durée totale du projet, nous sommes à 210h de travail par personne en moyenne.


Conclusion
---
### Application Godot

L'application a rencontré plusieurs modifications comme le système de récompense ou encore la manière de détecter la bonne prononciation d'un mot. L'application est aussi prête à recevoir les différentes listes de mots personnalisées en fonction de chaque patient notamment grâce au système d'authentification. Il reste néanmoins certaines choses à faire par rapport aux différentes demandes du client comme le déplacement du jeu "Ecoute et choisis" en mode phonétique ou encore gérer le format de la date dans l'émission des résultats.

### Interface Administrateur

Lors de ce projet, nous aurons réussi à créer différentes pages pour les orthophonistes. En effet, nous avons développé une page listant ses patients, une interface pour rajouter un patient, une interface pour rajouter un mot dans le dictionnaire ainsi qu'une page pour créer une liste de mot pour un patient. Du point de vue interface, nous aurons donc réussi à répondre à l'ensemble des demandes du cahier des charges. Cependant, ces différentes interfaces n'ont pas pu être reliées à notre serveur backend et ne peuvent donc pas intéragir avec la base de données. Ce sera la prochaine étape du développement d'Artiphonie, à savoir : faire communiquer l'interface web d'administration et l'API.

### Backend

Toutes les API pour les tables enfant et orthophoniste sont fonctionnelles. De plus, il est possible d'obtenir les résultats de tous les patients.

La migration du backend avec de nouvelles technos ainsi que la correspondance avec la base de données SQLite grâce à SQLAlchemy a pris beaucoup de temps. Le backend n'a été fonctionnel que très tard dans le projet. Ainsi, les API principales ont été développées mais beaucoup restent non réalisées ou inexploitables. Le temps nécessaire à l'apprentissage des technos a été sous-estimé ce qui a engendré un certain retard dans le projet. 


Retour d'expérience
---
- **Julien** : 
Ce projet a été personellement enrichissant sur plusieurs aspects. Le premier est sans aucun doute le fait d'avoir pu travailler en collaboration avec un client d'un corps de métier totalement différent, de découvrir toutes les facettes et enjeux de la communication avec ce dernier. Savoir écouter, comprendre et savoir apporter une réponse adapté aux connaissances du client m'a particulièrement plu. L'aspect social du projet m'a beaucoup motivé, savoir que ce projet avait une vrai utilité et pas des moindre a été un élément moteur pour moi. Le dernier élément que je citerai est le fait d'avoir travailler avec des personnes avec qui je n'avais encore jamais collaborer, cela a été instructif car savoir s'adapter à la diversité des caractères ou comportements est selon moi une qualité essentielle pour un chef de projet. Un des rares point négatif est que je pense qu'avec un peu plus de temps, nous aurions pu vraiment proposer un produit utilisable.  

- **Paul** : 
Mon retour sur ce projet est globalement très positif. Ce projet dès que je l'ai vu m'a attiré. En effet, j'ai une affinité particulière avec l'orthophonie. Je n'ai pas été déçu de choisir ce projet, j'ai pu échanger avec une orthophoniste sur des sujets auxquels je n'ai pas l'habitude de travailler. Cela a été très enrichissant.
De plus en début de projet j'ai décidé d'endosser le rôle de Product Owner dans l'intention d'avoir une vision globale. Je suis satisfait et fier d'avoir réussi à tenir ce rôle. J'ai pu garder tout au long du projet une vision d'ensemble et j'ai pu m'impliquer sur tous les aspects du projet.
Malheureusement, je suis déçu de rendre le projet en l'état actuel. Nous avons fait réellement avancer le projet, mais pas assez, à mon goût, pour satisfaire les besoins de la cliente.
Ce projet m'a tellement plus que je suis prêt à continuer de contribuer au projet pendant mon temps libre.   

- **Sami** :
J'ai beaucoup aimé travaillé sur le projet Artiphonie. Le travail avec mon équipe etait très bien et j'ai beaucoup aimé les rendez-vous avec la cliente. Cette dernière est très impliquée dans le projet et c'est vraiment agréable de travailler avec elle. Je pense qu'il nous aurait fallu une semaine de plus pour implémenter certaines fonctionnalités. J'ai passé énormément de temps sur SQLAlchemy et le backend en général. J'aurais voulu m'essayer à d'autres aspect du projet plutôt qu'uniquement le back. Hélas, le temps nous était compté.  

- **Daphné** :  
Le projet Artiphonie a apporté des aspects nouveaux au travail d'ingénieur en informatique, celui que j'ai pu apprendre au cours de nos nombreux projets en 3 années à Polytech Grenoble.  
Par sa proximité avec la partie Client, j'ai appris l'importance de la compréhension et de l'humilité de l'ingénieur en informatique, face aux exigences et contraintes techniques abordées. Une interface, comme une fonctionnalité, n'est bonne que si elle correspond tout à fait, ou dans la mesure du réalisable pour le développeur, à ce que souhaite le client.  
Par sa durée, j'ai appris à cerner mon rythme de travail et mes motivations au plus tôt.  
Par la présence d'une équipe humaine, j'ai consolidé mes compétences sociales, en équipe.  
Bien sûr et par dessus tout, par la mission que ce projet servait, je me suis sentie utile et investie à faire de mon mieux dans le temps qui m'était impartie.  
J'ai beaucoup aimé l'organisation et la rigueur que ce projet exigeait, tant dans la tenue d'une documentation bien faite, que dans la prise en main rapide des technologies et exigences, pour coller au mieux à l'emploi du temps demandé. J'ai aimé me retrouver seule sur la partie Vue de l'interface d'administration. Cette position était confortable et me permettait d'aller vite. Néanmoins, je pense que ce n'est pas une position saine sur le long terme ; car j'aurai vite eu besoin d'un deuxième regard, et d'une deuxième réflexion à ma tâche.  
Je suis très fière du travail de chacun, malgré la déception de ne pas avoir pu atteindre les objectifs initiaux. Savoir que l'application devra attendre 1 an supplémentaire, ou peut-être plus, avant sa mise en pratique, ...


Bibliographie
---

* [Artiphonie saison 2](https://air.imag.fr/index.php/Artiphonie_(saison_2))
* [Tutoriel VueJS+FastAPI+Docker](https://testdriven.io/blog/developing-a-single-page-app-with-fastapi-and-vuejs/)
* [Tutoriel FastAPI et SQLAlchemy](https://fastapi.tiangolo.com/tutorial/sql-databases/)
* [Documentation Godot](https://docs.godotengine.org/en/stable/)  
* [Librairie de composants PrimeVue](https://primefaces.org/primevue/#/)
* [Librairie PrimeFlex pour des systèmes de grilles responsive en Vue](https://www.primefaces.org/primeflex/)
