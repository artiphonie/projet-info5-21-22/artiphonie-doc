Spécification de la nouvelle Base de données
============
_Artiphonie version 3_

----------

**objet** (Table principale) : Représente l’entité Objet que l’enfant peut débloquer pour personnaliser son avatar en gagnant des étoiles sur l’application Artiphonie

* id (clé primaire) : int, Identifiant de l’objet  
* nom_objet : string, Nom donné à l’objet  
* type : int, Définit le type de l’objet ( pull, chapeau …)  
* cout : int, Coût pour débloquer l’objet par l’enfant  

--------------------------------------------------------

**objet_enfant** (Table d'association) : Représente l’entité Object_enfant, soit un objet qu'un enfant a débloqué sur son compte

* id_objet (clé primaire), 
* id_enfant (clé primaire)

--------------------------------------------------------

**enfant** (Table principale) : Représente l’entité Enfant dans l’application Artiphonie.

* id (clé primaire) : int , Identifiant de l’enfant
* nom : string, Nom de l’enfant
* prenom : string, Prénom de l’enfant
* date_naissance : date, Date de naissance de l'enfant
* classe : string, Niveau scolaire de l'enfant
* nb_etoile : String, Le nombre d'étoiles dévérouillées par l'enfant
* id_ortho : int, Identifiant de l’orthophoniste auquel l’enfant est rattaché
* login : string, Login d’accès au compte de l’enfant
* password : string, Mot de passe d’accès au compte de l’enfant

id_ortho (table enfant) = id (table orthophoniste)  

----------------------------------------------------------------

**liste_enfant** (Table d'association) : Représente l’entité Liste_enfant, soit une liste de travail associée à un enfant à une date spécifique

* id_enfant (clé primaire),
* id_liste_filtre (clé primaire) : int , Identifiant de la liste de filtre,
* date (clé primaire): Date, la date à laquelle l'enfant a été associé à la liste

--------

**resultat** (Table d'association) : Représente l'entité Résultat d'un enfant sur une liste de mot spécifique, après plusieurs tentatives

* id_enfant (clé primaire)
* id_liste_filtre (clé primaire)
* date (clé primaire), Date, la date à laquelle l'enfant s'est exercé sur la liste
* nb_tentative : int, le nombre de fois où l'enfant a fait une activité utilisant la liste
* nb_erreur : int, le nombre d'erreurs commises lors des tentatives de l'enfant
* commentaire : string, éventuelles remarques sur la performance de l'enfant.
* nom_jeu : string, le nom du jeu sur lequel l'enfant s'est exercé
* difficulte : int, le niveau de difficulté du jeu associé à la tentative de l'enfant
* taux_reussite : float, pourcentage de réussite de l'enfant sur sa tentative
* mot_rates : array, la liste des mots que l'enfant n'a pas réussi à prononcer.

--------------------------------------------------------

**liste_filtre** (Table principale) : Représente l’entité Liste_filtre dans l’application Artiphonie, qui permettra de générer une collection de mots sur-mesure pour l'enfant. On utilisera courramment le terme de liste de mots pour parler du résultat généré par la liste de filtres.

* id (clé primaire) : int , Identifiant de la liste de filtre
* nom : String, Nom de la liste
* nb_syllabe : int, Nombre de syllabe dans la liste
* structure_syllabique : Array[boolean], Contient toutes les différentes structures recensées par l'orthophoniste et effectives pour une liste
* structure_syllabique_type : Array[boolean], Contient tous les types de la structure syllabique pour une liste (ex : consonne occlusive, voyelle nasale...)
* phoneme : Array[String], Le tableau de tous les phonèmes du langage API (alphabet phonétique international).

----------------------------------------------------------------

**orthophoniste** (Table principale) : Représente l’entité Orthophoniste dans l’application Artiphonie

* id (clé primaire) : int, Identifiant de l’orthophoniste
* nom : String, Nom de l’orthophoniste
* prenom : String, Prénom de l’orthophoniste
* mail : String, Email pour contacter l’orthophoniste
* login : String, Login de l’orthophoniste pour accéder à son compte
* password : String, Mot de passe de l’orthophoniste pour accéder à son compte

--------

**mot** (Table principale) : Représente l'entité Mot utilisé pour l’apprentissage des enfants partout dans l'application Artiphonie

* id ( clé primaire): int, Représente l'identifiant du mot
* orthographe: String, Orthographe du mot
* nb_syllabe: int, Nombre de syllabe
* structure_syllabique, structure grammaticale phonétique (ex pour le mot pain : "CoVn" avec Co pour consonne occlusive et Vn pour voyelle nasale)
* phonetique: String, Écriture phonetique du mot 
* id_image: String, Nom de l’image liée au mot

id_image (table mot) = id (table image)

--------

**mot_liste** (Table d'association) : Représente l'entité Mot_liste, contenu dans une liste de mots

* id_liste_filtre (clé primaire)
* id_mot (clé primaire)

--------------------------------------------------------

**image** (Table principale) : Représente l'entité Image, associé à un mot

* id ( clé primaire): int, l'identifiant de l'image
* nom : String, nom de l'image
* id_mot : int, identifiant du mot à laquelle est reliée une image
* description : String, Bref descriptif de l’image
* image: bytearray, L’image sous forme de tableau d’octets
* type : Spring, Type d’image comme par exemple une image illustrative, d’aide à la prononciation...

id_mot (table image) = id (table mot)
  