Rapport technique
================================================================
_Vous trouverez dans ce rapport l'ensemble des documents annexes au Rapport final, et qu'il nous a semblé pertinent de transmettre_

[Etude de Conception de l'interface d'administration](./ConceptionInterfaceAdmin/Conception.md) 
----------------------------------------------------------------

Etant la première équipe à développer l'interface servant aux orthophonistes à administrer leurs patients, nous avons passé du temps à la concevoir, en utilisant les techniques de conception vues en cours.  

Ce document retrace l'ensemble de nos réflexions et productions concernant cette plateforme web.  

[Justification du choix de PrimeVue pour Vue3](./ConceptionInterfaceAdmin/UIComponentLibraryJustification.md)
------------------------------------------------

Il a été décidé que l'interface d'administration serait développée en Vue, un langage récent, ayant les particularités d'être simple et léger. Nous avons choisi la version 3, dans l'optique de rendre l'application évolutive pour les prochaines équipes qui reprendront ce travail.  

De ce fait, nous avons dû choisir une librairie de composants compatible avec Vue3 et adaptée au projet. Une étude de marché sommaire, et nos critères de choix ont été détaillés dans ce document.  

[Spécification de la base de données](./Sp%C3%A9cificationBDD.md)
--------------------------------------------------------

Voici la nouvelle base de données conçue pour s'adapter aux nouvelles volontés du client, comme la fonctionnalité de personnalisation des listes de mot par enfant, ou la possibilité de visualiser les résultats de l'enfant à distance, depuis la plateforme web.  

![Diagramme nouvelle BDD](./Rapport%20final%20-%20Images/Diagramme%20nouvelle%20BDD.png)
  
Ce document détaille les spécifications de chaque table ci-dessus. 

[Poursuite du travail](./Poursuite%20du%20travail.md) 
-------------

Ce document reprend les différentes volontés du client, évoquées durant la version 3, mais non implémentées par faute de temps.  

C'est un document qui se veut utile à la prochaine équipe d'étudiants qui se penchera sur le projet, afin de les aider à mieux cerner les enjeux à venir.